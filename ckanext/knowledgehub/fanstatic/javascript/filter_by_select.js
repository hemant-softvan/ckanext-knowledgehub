$('#organization-package-filter').on("select2-opening", function() {
        var url = new URL(window.location.href);
        var organizationParam = url.searchParams.get("organization");
       if(organizationParam != null){
         $("#organization-package-filter").select2({minimumResultsForSearch: -1});
       }
})

$('#organization-package-filter').on('select2-removing select2-selecting',
function (e) {
    window.location.href = e.val;
});

$('#tags-package-filter').on('select2-removing select2-selecting', function (e) {
        window.location.href = e.val;
});

$('#tags-filter').on('select2-removing select2-selecting', function (e) {
        window.location.href = e.val;
});

$('#organization-filter').on('select2-removing select2-selecting', function (e) {
    window.location.href = e.val;
});

$('#organizations-filter').on('select2-removing select2-selecting', function (e) {
    window.location.href = e.val;
});