(function (_, jQuery) {

let page = 1;
let limit = 10;
let rightSliderCount = 5;
let offset = (page - 1) * limit
let totalOutputResults = 0

var dashboardId = document.getElementById('dashboard-id').value;
var idxTags = document.getElementById('dashboard-tags').value;
var sharedWithDatasets = document.getElementById('dashboard-datasets').value;
var sharedWithOrganizations = document.getElementById('dashboard-shared_with_organizations').value;
var dashboardCards = document.getElementById('dashboard-cards');


let dashboardCardHtml = `<div class="dashboard-card">
                        <div class="dashboard-title-box related-dashboard-title">
                          <h4 class="component-heading">
                            <a href="/dashboards/{%DASHBOARDNAME%}/view">{%DASHBOARDTITLE%}</a>
                          </h4>
                          <a href="/dashboards/edit/{%DASHBOARDNAME%}" title="Manage" class="dashboard-manage-icon">
                            <i class="fa fa-wrench" aria-hidden="true"></i>
                          </a>
                        </div>
                        <div class="related-dashboard-description">{%DASHBOARDDESCRIPTION%}</div>
                    </div>`

let noDataFoundHtml = `<div class="no-dashboard-found-heading">
                            No related dashboard found.
                        </div>`


function makeDashboardHtmlCard(dashboard, htmlTemp) {
    let output = htmlTemp.replace(/{%DASHBOARDNAME%}/g, dashboard.name);
    output = output.replace(/{%DASHBOARDTITLE%}/g, dashboard.title);
    output = output.replace(/{%DASHBOARDDESCRIPTION%}/g, dashboard.description);
    return output
}

function resultFun(result) {
    if (result.data.length === 0 && page === 1) {
        dashboardCards.insertAdjacentHTML('afterbegin', noDataFoundHtml)
    }
    else if (result.data.length > 0) {
        const outputTemp = result.data.map(el => makeDashboardHtmlCard(el, dashboardCardHtml)).join('');
        dashboardCards.insertAdjacentHTML('beforeend', outputTemp)
        totalOutputResults = result.total;
        makeCardSlider();
    }
}



var getRelatedDashboards = function(data, success, error) {
                $.ajax({
                    'url': '/api/3/action/related_dashboard_list',
                    'method': 'POST',
                    'data': data,
                    'dataType': 'json',
                    'success': function(resp){
                        if (success){
                            success(resp.result)
                        }
                    },
                    'error': error
                })
            }


let data = {
    'page': page,
    'limit': limit,
    'offset': offset,
    'shared_with_datasets': sharedWithDatasets ? sharedWithDatasets : '',
    'shared_with_organizations': sharedWithOrganizations ? sharedWithOrganizations: '',
    'idx_tags': idxTags ? idxTags : '',
    'dashboard_id': dashboardId
}


getRelatedDashboards(data, (result) => resultFun(result))


function updateSliderArrowsStatus(
  cardsContainer,
  containerWidth,
  cardCount,
  cardWidth
) {
  if (
    $(cardsContainer).scrollLeft() + containerWidth <
    cardCount * cardWidth + 15
  ) {
    $("#slide-right-container").addClass("active");
  } else {
    $("#slide-right-container").removeClass("active");
  }
  if ($(cardsContainer).scrollLeft() > 0) {
    $("#slide-left-container").addClass("active");
  } else {
    $("#slide-left-container").removeClass("active");
  }
}
function makeCardSlider() {
  // Scroll products' slider left/right
  let div = $("#dashboard-cards-container");
  let cardCount = $(div)
    .find(".dashboard-cards")
    .children(".dashboard-card").length;
  let speed = 1000;
  let containerWidth = $(".dashboard-scroll-container").width();
  let cardWidth = 250;

  updateSliderArrowsStatus(div, containerWidth, cardCount, cardWidth);

  //Remove scrollbars
  $("#slide-right-container").unbind("click").click(function(e) {
    if ($(div).scrollLeft() + containerWidth < cardCount * cardWidth) {
      $(div).animate(
        {
          scrollLeft: $(div).scrollLeft() + cardWidth
        },
        {
          duration: speed,
          complete: function() {
            setTimeout(
              updateSliderArrowsStatus(
                div,
                containerWidth,
                cardCount,
                cardWidth
              ),
              1005
            );
          }
        }
      );
      rightSliderCount = rightSliderCount + 1
      if ((limit * page - 1) === rightSliderCount && totalOutputResults >= page * limit) {
         page = page + 1;
         data['page'] = page;
         data['offset'] = (page - 1) * limit
         getRelatedDashboards(data, (result) => resultFun(result))
      }
    }
    updateSliderArrowsStatus(div, containerWidth, cardCount, cardWidth);
  });
  $("#slide-left-container").unbind("click").click(function(e) {
    if ($(div).scrollLeft() + containerWidth > containerWidth) {
      $(div).animate(
        {
          scrollLeft: "-=" + cardWidth
        },
        {
          duration: speed,
          complete: function() {
            setTimeout(
              updateSliderArrowsStatus(
                div,
                containerWidth,
                cardCount,
                cardWidth
              ),
              1005
            );
          }
        }
      );
    }
    rightSliderCount = rightSliderCount - 1
    updateSliderArrowsStatus(div, containerWidth, cardCount, cardWidth);
  });

  // If resize action ocurred then update the container width value
  $(window).resize(function() {
    try {
      containerWidth = $("#dashboard-cards-container").width();
      updateSliderArrowsStatus(div, containerWidth, cardCount, cardWidth);
    } catch (error) {
      console.log(
        `Error occured while trying to get updated slider container width:
            ${error}`
      );
    }
  });
};


})(ckan.i18n.ngettext, $);