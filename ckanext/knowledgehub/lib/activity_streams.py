# encoding: utf-8

import re

from webhelpers.html import literal

import ckan.lib.helpers as h
import ckan.lib.base as base
import ckan.logic as logic

from ckan.common import _, is_flask_request


# get_snippet_*() functions replace placeholders like {user}, {dataset}, etc.
# in activity strings with HTML representations of particular users, datasets,
# etc.
def get_snippet_datarequest(activity, detail):
    return literal('''<span>%s</span>'''
                   % ('<a href="/datarequest/{}">{}</a>'.format(activity["id"], activity["title"]))
                   )


def get_snippet_admin(activity, detail):
    return literal('''<span class="actor">%s</span>'''
                   % (h.linked_user(activity['assign_by'], 0, 30))
                   )


# activity_stream_string_*() functions return translatable string
# representations of activity types, the strings contain placeholders like
# {user}, {dataset} etc. to be replaced with snippets from the get_snippet_*()
# functions above.

def activity_stream_string_assign_admin(context, activity):
    return _("{admin} has assigned you a data request {datarequest}")


# A dictionary mapping activity snippets to functions that expand the snippets.
activity_snippet_functions = {
    'datarequest': get_snippet_datarequest,
    'admin': get_snippet_admin
}

# A dictionary mapping activity types to functions that return translatable
# string descriptions of the activity types.
activity_stream_string_functions = {
    'assign admin': activity_stream_string_assign_admin
}

# A dictionary mapping activity types to the icons associated to them
activity_stream_string_icons = {
    'undefined': 'certificate',  # This is when no activity icon can be found
    'assign admin': 'user'
}

# A list of activity types that may have details
activity_stream_actions_with_detail = ['changed package']


def activity_list_to_html(context, activity_stream, extra_vars):
    """Return the given activity stream as a snippet of HTML.

    :param activity_stream: the activity stream to render
    :type activity_stream: list of activity dictionaries
    :param extra_vars: extra variables to pass to the activity stream items
        template when rendering it
    :type extra_vars: dictionary

    :rtype: HTML-formatted string

    """
    activity_list = []  # These are the activity stream messages.
    for activity in activity_stream:
        detail = None
        activity_type = activity['activity_type']
        # Some activity types may have details.
        if activity_type in activity_stream_actions_with_detail:
            details = logic.get_action('activity_detail_list')(context=context,
                                                               data_dict={'id': activity['id']})
            # If an activity has just one activity detail then render the
            # detail instead of the activity.
            if len(details) == 1:
                detail = details[0]
                object_type = detail['object_type']

                if object_type == 'PackageExtra':
                    object_type = 'package_extra'

                new_activity_type = '%s %s' % (detail['activity_type'],
                                               object_type.lower())
                if new_activity_type in activity_stream_string_functions:
                    activity_type = new_activity_type

        if activity_type not in activity_stream_string_functions:
            raise NotImplementedError("No activity renderer for activity "
                                      "type '%s'" % activity_type)

        if activity_type in activity_stream_string_icons:
            activity_icon = activity_stream_string_icons[activity_type]
        else:
            activity_icon = activity_stream_string_icons['undefined']

        activity_msg = activity_stream_string_functions[activity_type](context,
                                                                       activity)

        # Get the data needed to render the message.
        matches = re.findall('\{([^}]*)\}', activity_msg)
        data = {}
        for match in matches:
            snippet = activity_snippet_functions[match](activity, detail)
            data[str(match)] = snippet

        activity_list.append({'msg': activity_msg,
                              'type': activity_type.replace(' ', '-').lower(),
                              'icon': activity_icon,
                              'data': data,
                              'timestamp': activity['timestamp'],
                              'is_new': activity.get('is_new', False)})
    extra_vars['activities'] = activity_list

    # TODO: Do this properly without having to check if it's Flask or not
    if is_flask_request():
        return base.render('activity_streams/activity_stream_items.html',
                           extra_vars=extra_vars)
    else:
        return literal(base.render('activity_streams/activity_stream_items.html',
                                   extra_vars=extra_vars))
