from sqlalchemy import (
    types,
    Column,
    Table,
    or_,
    ForeignKey,
    func,
    and_
)

from ckan.model.meta import (
    metadata,
    mapper,
    engine,
    Session
)
from ckan.model.types import make_uuid
from ckan.model.domain_object import DomainObject

datarequests_table = Table('datarequests', metadata,
                           Column('user_id', types.UnicodeText, primary_key=False, default=u''),
                           Column('id', types.UnicodeText, primary_key=True, default=make_uuid),
                           Column('title', types.Unicode(100), primary_key=True,
                                  default=u''),
                           Column('description', types.Unicode(1000),
                                  primary_key=False, default=u''),
                           Column('organization_id', types.UnicodeText, primary_key=False,
                                  default=None),
                           Column('open_time', types.DateTime, primary_key=False, default=None),
                           Column('accepted_dataset_id', types.UnicodeText, primary_key=False,
                                  default=None),
                           Column('close_time', types.DateTime, primary_key=False, default=None),
                           # We are not using in new data request just for old data mapping
                           Column('closed', types.Boolean, primary_key=False, default=False),
                           Column('extras', types.UnicodeText, primary_key=False),
                           Column('visibility', types.Integer, default=0),
                           # Added data_requests_status column in db
                           Column('data_requests_status', types.Integer, primary_key=False, default=0),
                           Column('assign_admin', types.UnicodeText, primary_key=False, default=None),
                           # accepted_dataset_id is resource or not
                           Column('is_resource', types.Boolean, primary_key=False, default=False),
                           Column('accepted_dashboard_id', types.UnicodeText, primary_key=False,
                                  default=None),
                           Column('display_dataset_or_resource', types.UnicodeText, primary_key=False,
                                  default=u''),
                           Column('display_dashboard', types.UnicodeText, primary_key=False,
                                  default=u''),
                           Column('assign_by', types.UnicodeText, primary_key=False,
                                  default=None),
                           Column('assign_time', types.DateTime, primary_key=False, default=None)
                           , extend_existing=True)


class DataRequest(DomainObject):

    @classmethod
    def get_assigned_admin(cls, assign_admin):
        """Looks up an assign datarequest by its name.

        :param assign_admin: `str`, assign admin name.

        :returns: `DataRequest` object or `None`.
        """
        q = Session.query(cls)
        q = q.filter(and_(datarequests_table.c.assign_admin == assign_admin,
                          datarequests_table.c.assign_by != None)).order_by(
                          datarequests_table.c.assign_time.desc())
        return q.all()


mapper(DataRequest, datarequests_table)


# def setup():
#     metadata.create_all(engine)
